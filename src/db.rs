use crate::models::{TodoList, TodoItem};
use deadpool_postgres::Client;
use std::io;
use tokio_pg_mapper::FromTokioPostgresRow;

pub async fn get_todos(client: &Client) -> Result<Vec<TodoList>, io::Error> {
	let statment = client.prepare("SELECT * FROM todo_list ORDER BY id DESC").await.unwrap();

	let todos = client.query(&statment, &[])
		.await
		.expect("Error getting todos")
		.iter()
		.map(|row| TodoList::from_row_ref(row).unwrap())
		.collect::<Vec<TodoList>>();

	Ok(todos)
}

pub async fn get_items(client: &Client, list_id: i32) -> Result<Vec<TodoItem>, io::Error> {
	let statment = client.prepare("SELECT * FROM todo_item where list_id = $1 ORDER BY id").await.unwrap();

	let items = client.query(&statment, &[&list_id])
		.await
		.expect("Error getting items")
		.iter()
		.map(|row| TodoItem::from_row_ref(row).unwrap())
		.collect::<Vec<TodoItem>>();

	Ok(items)
}

pub async fn create_todo(client: &Client, title: String) -> Result<TodoList, io::Error> {
	let statment = client.prepare("INSERT INTO todo_list (title) values ($1) returning id, title").await.unwrap();

	client.query(&statment, &[&title])
		.await
		.expect("Error creating todo list")
		.iter()
		.map(|row| TodoList::from_row_ref(row).unwrap())
		.collect::<Vec<TodoList>>()
		.pop()
		.ok_or(io::Error::new(io::ErrorKind::Other, "Error creating todo list"))
}

pub async fn toggle_todo(client: &Client, item_id: i32) -> Result<TodoItem, io::Error> {
	let statment = client.prepare("UPDATE todo_item SET checked = NOT checked WHERE id = $1 returning id, title, checked, list_id").await.unwrap();

	client.query(&statment, &[&item_id])
		.await
		.expect("Error toggle todo item")
		.iter()
		.map(|row| TodoItem::from_row_ref(row).unwrap())
		.collect::<Vec<TodoItem>>()
		.pop()
		.ok_or(io::Error::new(io::ErrorKind::Other, "Error toggle todo item"))
}

