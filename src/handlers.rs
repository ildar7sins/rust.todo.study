use crate::db;
use crate::models::{Status, CreateTodoList};
use deadpool_postgres::{Client, Pool};
use actix_web::{web, get, put, Responder, HttpResponse};

pub async fn status() -> impl Responder {
	web::HttpResponse::Ok().json(Status{status: "OK".to_string()})
}

pub async fn get_todos(db_pool: web::Data<Pool>) -> impl Responder {
	let client: Client = db_pool.get().await.expect("Error connecting to DB");

	println!("Hello server :)");

	let result = db::get_todos(&client).await;

	match result {
		Ok(todos) => HttpResponse::Ok().json(todos),
		Err(_) => HttpResponse::InternalServerError().into()
	}
}

#[get("/todos/{list_id}/items")]
pub async fn get_items(db_pool: web::Data<Pool>, web::Path(list_id): web::Path<i32>) -> impl Responder {

	let client: Client = db_pool.get().await.expect("Error connecting to DB");

	let result = db::get_items(&client, list_id).await;

	match result {
		Ok(items) => HttpResponse::Ok().json(items),
		Err(_) => HttpResponse::InternalServerError().into()
	}
}

pub async fn create_todo(db_pool: web::Data<Pool>, _data: web::Json<CreateTodoList>) -> impl Responder {
	let client: Client = db_pool.get().await.expect("Error connecting to DB");

	let result = db::create_todo(&client, _data.title.to_string()).await;

	match result {
		Ok(items) => HttpResponse::Ok().json(items),
		Err(_) => HttpResponse::InternalServerError().into()
	}
}

#[put("/items/{id}")]
pub async fn check_item(db_pool: web::Data<Pool>, web::Path(id): web::Path<i32>) -> impl Responder {
	let client: Client = db_pool.get().await.expect("Error connecting to DB");

	let result = db::toggle_todo(&client, id).await;

	match result {
		Ok(items) => HttpResponse::Ok().json(items),
		Err(_) => HttpResponse::InternalServerError().into()
	}
}
